const commonPlugins = [
  [
    require.resolve('babel-plugin-module-resolver'),
    {
      root: ['./'],
      alias: {
        '#screens': './src/screens',
        '#containers': './src/containers',
        '#presentations': './src/presentations',
        '#icons': './src/presentations/icons',
        '#navigation': './src/navigation',
        '#services': './src/services',
        '#redux': './src/store/redux',
        '#selectors': './src/store/selectors',
        '#constants': './src/core/constants',
        '#utils': './src/core/utils',
        '#styles': './src/core/styles',
        '#tests': './__tests__',
      },
    },
  ],
];

const productionPlugins = [
  ['transform-remove-console', { exclude: ['error', 'warn'] }],
];

module.exports = function (api) {
  const config = {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [...commonPlugins],
  };

  if (api.env('production')) {
    config.plugins = [...config.plugins, ...productionPlugins];
  }

  return config;
};
