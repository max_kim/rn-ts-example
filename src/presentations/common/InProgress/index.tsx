import React from 'react';
import styled from 'styled-components/native';

export const InProgress: React.FC = () => {
  return (
    <Container>
      <Text>In progress... 🤑</Text>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Text = styled.Text`
  font-size: 28px;
`;
