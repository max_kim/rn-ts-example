export const screens = Object.freeze({
  stack: {
    authStack: 'AUTH_STACK',
    mainStack: 'MAIN_STACK',
  },
  authStack: {
    stack: {
      mock: 'MOCK',
    },
  },
  mainStack: {
    stack: {
      mock: 'MOCK',
    },
  },
});
