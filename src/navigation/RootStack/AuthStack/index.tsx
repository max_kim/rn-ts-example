import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { screens } from '#navigation/screens';

import { InProgress } from '#presentations/common/InProgress';

import { withoutHeader } from '#navigation/options';

const Stack = createStackNavigator();

export const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...withoutHeader,
      }}>
      <Stack.Screen
        name={screens.authStack.stack.mock}
        component={InProgress}
      />
    </Stack.Navigator>
  );
};
