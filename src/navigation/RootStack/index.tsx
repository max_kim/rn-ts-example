import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { AuthStack } from './AuthStack';
import { MainStack } from './MainStack';

import { screens } from '#navigation/screens';
import { withoutHeader } from '#navigation/options';

const Stack = createStackNavigator();

export const RootStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...withoutHeader,
      }}>
      <Stack.Screen name={screens.stack.authStack} component={AuthStack} />
      <Stack.Screen name={screens.stack.mainStack} component={MainStack} />
    </Stack.Navigator>
  );
};
