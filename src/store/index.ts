// @flow
import { configureStore } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import createReduxDebugger from 'redux-flipper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { rootReducer } from './redux';

import type { Persistor, PersistConfig } from 'redux-persist/es/types';
import type { Store } from 'redux';

import type { RootState } from './redux';

const persistConfig: PersistConfig<RootState> = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store: Store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware => {
    const defaultMiddleware = getDefaultMiddleware({
      serializableCheck: {
        // Ignore these action types
        ignoredActions: ['persist/PERSIST'],
      },
    });

    if (__DEV__) {
      console.log('#########');
      console.log('createReduxDebugger');
      console.log('#########');
      return defaultMiddleware.concat(createReduxDebugger());
    }

    return defaultMiddleware;
  },
  devTools: __DEV__,
});

export const persistor: Persistor = persistStore(store);
