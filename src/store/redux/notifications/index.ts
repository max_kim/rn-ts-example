import { createSlice } from '@reduxjs/toolkit';

export interface Notification {
  id: string;
  title: string;
  body: string;
}

export interface NotificationsState {
  list: Notification[];
  current: Notification | null;
}

const initialState: NotificationsState = {
  list: [],
  current: null,
};

export const notificationsSlice = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    addNotification: (state: NotificationsState, action) => {
      state.list = [action.payload, ...state.list];
    },
    removeNotification: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    clearNotificationsList: state => {
      state.list = [];
      state.current = null;
    },
    setCurrentNotification: (state, action) => {
      state.current = action.payload;
    },
  },
});

export const notificationsActions = notificationsSlice.actions;

export const notifications = notificationsSlice.reducer;
