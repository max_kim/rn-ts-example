import { combineReducers } from 'redux';

import { notifications } from './notifications';

export const rootReducer = combineReducers({
  notifications,
});

export type RootState = ReturnType<typeof rootReducer>;
